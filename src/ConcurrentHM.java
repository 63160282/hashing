import java.util.concurrent.*;

public class ConcurrentHM {

	public static void main(String[] args){
		ConcurrentHashMap<Integer, String> m = new ConcurrentHashMap<Integer, String>();
		m.put(100, "Hello");
		m.put(101, "Geeks");
		m.put(102, "Geeks");

		// Print ConcurrentHashMap
		System.out.println("ConcurentHashMap: " + m);

		m.putIfAbsent(101, "Hello");

		// Printing the ConcurrentHashMap
		System.out.println("ConcurentHashMap: " + m);

		m.remove(101, "Geeks");

		// Print ConcurrentHashMap
		System.out.println("\nConcurentHashMap: " + m);

		m.replace(100, "Hello", "For");

		// Print ConcurrentHashMap
		System.out.println("\nConcurentHashMap: " + m);
	}
}

